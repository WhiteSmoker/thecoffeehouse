// You can import Ionicons from @expo/vector-icons if you use Expo or
// react-native-vector-icons/Ionicons otherwise.
import * as React from 'react';
import { Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/screens/Home'
import Login from './src/screens/Login'
import Order from './src/screens/Order'
import Store from './src/screens/Store'
import AccumulatePoints from './src/screens/AccumulatePoints'
import Setting from './src/screens/Setting'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
// function HomeStack() {
//   return (
//     <Stack.Navigator initialRouteName="Home">
//         <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
//         <Stack.Screen name="ProductList" component={ProductList} options={{ title: "Women's Best Sellers ", headerTitleStyle: {fontSize: 22, fontWeight:'bold', marginLeft:50}}}/>
//         <Stack.Screen name="Detail" component={Detail} options={{ title: "FOREVER 21 ",headerTitleStyle: {fontSize: 22, fontWeight:'bold', marginLeft:85},}}/>
//       </Stack.Navigator>
//   );
// }

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            

            if (route.name === 'Trang Chủ') {
              iconName = focused ? 'home-outline' : 'home-outline';
            } else if (route.name === 'Đặt Món') {
              iconName = focused ? 'cart-outline' : 'cart-outline';
            } else if (route.name === 'Cửa Hàng') {
              iconName = focused ? 'briefcase-outline' : 'briefcase-outline';
            } else if (route.name === 'Tích Điểm') {
              iconName = focused ? 'gift-outline' : 'gift-outline';
            } else if (route.name === 'Khác') {
              iconName = focused ? 'md-menu' : 'md-menu';
            }


            // You can return any component that you like here!
            return <Ionicons name={iconName} size={30} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: '#cc8610',
          inactiveTintColor: 'grey',
        }}
      > 
        <Tab.Screen name="Trang Chủ" component={Home}/>
        <Tab.Screen name="Đặt Món" component={Order} />
        <Tab.Screen name="Cửa Hàng" component={Store} />
        <Tab.Screen name="Tích Điểm" component={AccumulatePoints} />
        <Tab.Screen name="Khác" component={Setting} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
