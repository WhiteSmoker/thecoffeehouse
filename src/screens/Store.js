import React, {useEffect, useState} from 'react'
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import stylesStore from '../style/StyleStore'
import {getAllStore} from '../services/Api'
import Ionicons from "react-native-vector-icons/Ionicons";
export default function Store() {
    const[store, setAllStore] = useState()
    useEffect(() => {
     const getApiAllStore = async()=>{
      const result = await getAllStore()
      console.log('result', result)
      setAllStore(result.data)
     }
    getApiAllStore()
    }, [])
    const renderItem = ({ item }) => (
        <TouchableOpacity style={stylesStore.listStore}>
            <Image style={stylesStore.imgStore} source={{ uri:item.image_1}}/>
            <View style={stylesStore.infoStore}>
                <Text style={stylesStore.textTitleApp}>THE COFFEE HOUSE</Text>
                <Text numberOfLines={2} style={stylesStore.textAdress}>{item.street}, {item.district_name}, {item.state_name}, {item.country}</Text>
            </View>
        </TouchableOpacity>
    );
    const allStore=()=>{
        return(
          <View style={stylesStore.flatlistAllStore}>
              <Text style={stylesStore.titleAllStore}>CÁC CỬA HÀNG KHÁC</Text>
          </View>
        )
      }
    return (
        <View style={stylesStore.container}>
            <View style={stylesStore.header}>
                <Image style={stylesStore.logoApp} source={require('../images/home/logo.jpg')}/>
                <View style={stylesStore.menuItem}>
                    <TouchableOpacity style={stylesStore.itemSearch}>
                        <Ionicons name="search" size={20} color='grey'/>
                        <Text style={stylesStore.textSearch}>Tìm kiếm</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={stylesStore.itemMap}>
                        <Ionicons name="map-sharp" size={20} color='grey'/>
                        <Text style={stylesStore.textMap}>BẢN ĐỒ</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={stylesStore.content}>
             
                    <FlatList
                        data={store}
                        renderItem={renderItem}
                        keyExtractor={(item) => item.id}
                        style={{height:620 }}
                        ListHeaderComponent={allStore}
                    />
            </View>         
        </View>
    )
}
