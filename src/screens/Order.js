import React, {useEffect, useState} from 'react'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import {getOrder} from '../services/Api'
import { getImage } from '../services/getImage';
import Ionicons from "react-native-vector-icons/Ionicons";
import axios from 'axios'
import stylesOrder from '../style/StyleOrder'
import Fontisto from "react-native-vector-icons/Fontisto";

export default function Order() {
    const[order, setOrder] = useState()
    useEffect(() => {
     const getApiOrder = async()=>{
      const result = await getOrder()
      console.log('result', result)
      setOrder(result.data.data)
     }
    getApiOrder()
    }, [])

    const renderItem = ({ item }) => (
        
            <TouchableOpacity style={stylesOrder.listProduct}>
                <View style={stylesOrder.infoProduct}>
                    <Text style={stylesOrder.textName}>{item.product_name}</Text>
                    <Text numberOfLines={2} style={stylesOrder.textDescription}>{item.description}</Text>
                    <Text style={stylesOrder.textPrice}>{item.price}₫</Text>
                </View>
                <Image style={stylesOrder.imgProduct} source={{ uri:item.image}}/>
            </TouchableOpacity>
      );
      const selectMustTryFood=()=>{
        return(
          <View style={stylesOrder.flatlistMustTryFood}>
              <Text style={stylesOrder.titleSelect}>Món phải thử</Text>
          </View>
        )
      }
    return (
        <View style={stylesOrder.container}>
            
            <TouchableOpacity style={stylesOrder.header}>
                 <View style={stylesOrder.iconDelivery}>   
                    <Fontisto name="motorcycle" size={25} color='#009eb0'/>    
                </View>
                <View style={stylesOrder.headerChild}>
                    <View style={stylesOrder.titleHeader}>
                        <Text style={stylesOrder.textTitleHeader}>Giao tận nơi</Text>
                        <Ionicons name="chevron-down" size={20} />
                    </View>
                    <Text style={stylesOrder.textTitleHeaderChild}>Các món sẽ được giao đến địa chỉ của các bạn</Text>
                </View>
            </TouchableOpacity>
            <View style={stylesOrder.menuItem}>
                <TouchableOpacity style={stylesOrder.itemSelectType}>
                    <Text style={stylesOrder.textType}>Thực đơn</Text>
                    <Ionicons name="chevron-down" size={30} color='grey'/>
                </TouchableOpacity>
                <TouchableOpacity style={stylesOrder.itemSearch}>
                    <Ionicons name="search" size={30} color='grey'/>
                </TouchableOpacity>
                <TouchableOpacity style={stylesOrder.itemHeart}>
                    <Ionicons name="heart" size={30} color='grey'/>
                </TouchableOpacity>
            </View>
                <View>
                    <FlatList
                        data={order}
                        renderItem={renderItem}
                        keyExtractor={(item) => item._id?.toString()}
                        style={{height:600 }}
                        ListHeaderComponent={selectMustTryFood}
                    />
                </View>
        </View>

    );

}
