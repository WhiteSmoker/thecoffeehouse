import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import stylesHome from '../style/StyleHome'
import Carousel from 'react-native-snap-carousel';
import DataHome from '../data/DataHome';
import Fontisto from "react-native-vector-icons/Fontisto"
import Ionicons from "react-native-vector-icons/Ionicons"
import LinearGradient from 'react-native-linear-gradient';

export default function Home() {
    const renderItem = ({item}) => {
        return (
        <TouchableOpacity>
            <Image source={ item.photo} style={stylesHome.imgCarousel} />
        </TouchableOpacity>
        );};
        
        return (
            <View style={stylesHome.container}>
                <View style={stylesHome.header}>
                    <Image style={stylesHome.logoApp} source={require('../images/home/logo.jpg')}/>
                </View>
                
                
                <ScrollView style={stylesHome.content}>
                    <LinearGradient colors={['#6e4b00', '#fff2d6']} >
                    <View style={stylesHome.carouselContent}>
                        <Carousel
                        data={DataHome}
                        renderItem={renderItem}
                        sliderWidth={430}
                        itemWidth={400}
                        />
                    </View></LinearGradient> 
                    <View style={stylesHome.deliveryMethod}>
                        <TouchableOpacity style={stylesHome.deliveryMethodChild}>
                            <View style={stylesHome.iconDelivery}>
                                <Fontisto name="motorcycle" size={25} color='#009eb0'/>
                            </View>
                            <Text style={stylesHome.textDeliveryMethod}>Giao tận nơi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={stylesHome.deliveryMethodChild}>
                            <View style={stylesHome.iconAutomaticallyGrab}>
                                <Ionicons name="cafe" size={25} color='#f2963a'/>
                            </View>
                            <Text style={stylesHome.textDeliveryMethod}>Tự đến lấy</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={stylesHome.event}>
                        <Image source={ require('../images/home/home6.jpg')} style={stylesHome.imgEvent} />
                    </TouchableOpacity>
                    <View style={stylesHome.footer}>
                        <View style={stylesHome.titleNotify}>
                            <Text style={stylesHome.textTitleNotify}>Thông báo mới</Text>
                        </View>
                        <TouchableOpacity style={stylesHome.notify}>
                            <Image source={ require('../images/home/home7.jpg')} style={stylesHome.imgNotify} />
                            <View>
                                <Text style={{fontSize:19,}}>Chào bạn mới</Text>
                                <Text style={{color:'gray'}}>03/04/2021 08:41</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                   
                </ScrollView>
            </View>
        );
    
}

