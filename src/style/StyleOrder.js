
import { StyleSheet } from 'react-native'

const stylesOrder = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
    },
    header:{
        flexDirection:'row',
        backgroundColor:'white',
    },
    iconDelivery:{
        width:35,
        height:35,
        borderRadius:20,
        backgroundColor:'#9be9f2',
        alignItems:'center',
        justifyContent:'center',
        marginVertical:15,
        marginHorizontal:15,
    },
    headerChild:{
        flexDirection:'column',
        alignSelf:'center',
    },
    titleHeader:{
        flexDirection:'row',
    },
    textTitleHeader:{
        fontSize:17,
        fontWeight:'bold',
    },
    textTitleHeaderChild:{
        fontSize:16,
        color:'gray'
    },
    menuItem:{
        flexDirection:'row',
        marginVertical:15,
        marginHorizontal:15,
    },
    itemSelectType:{
        width:270,
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:'#dbdbdb',
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal:10,
        borderRadius:10,
    },
    textType:{
        fontSize:18,
    },
    itemSearch:{
        alignSelf:'center',
        backgroundColor:'#dbdbdb',
        paddingVertical:10,
        paddingHorizontal:10,
        borderRadius:10,
        marginHorizontal:15,
        
    },
    itemHeart:{
        alignSelf:'center',
        backgroundColor:'#dbdbdb',
        paddingVertical:10,
        paddingHorizontal:10,
        borderRadius:10,
    },
    listProduct:{
        backgroundColor:'white',
        flexDirection:'row',
        marginHorizontal:15,
        borderRadius:10,
        marginVertical:10,
        paddingHorizontal:10,
        paddingVertical:10,
        height:140,
        shadowColor: "#000",
        shadowOffset: {
	    width: 0,
	    height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
    },
    infoProduct:{
        width:'65%',
        marginVertical:10,
        marginHorizontal:5,
    },
    textName:{
        fontSize:18,
        fontWeight:'bold',
    },
    textDescription:{
        fontSize:17,
        color:'gray',
        marginBottom:15,
    },
    textPrice:{
        fontSize:18,
    },
    imgProduct:{
        width:'30%',
        height:110,
        alignSelf:'center',
        borderRadius:10,
    },


    flatlistMustTryFood:{
        marginHorizontal:17,
        marginVertical:10,
    },
    titleSelect:{
        fontSize:20,
        fontWeight:'bold',
    },
})
export default stylesOrder;