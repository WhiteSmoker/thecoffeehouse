import { StyleSheet} from 'react-native'
const stylesStore = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
    },
    header:{
        flexDirection:'column',
        paddingHorizontal:15,
        backgroundColor:'white',
    },
    logoApp:{
        width:151,
        height:27,
        marginVertical:20,
    },
    menuItem:{
        flexDirection:'row',
        marginBottom:10,
        justifyContent:'space-between',
    },
    itemSearch:{
        width:270,
        flexDirection:'row',
        backgroundColor:'#dbdbdb',
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal:10,
        borderRadius:10,
    },
    textSearch:{
        fontSize:18,
        color:'gray',
    },
    itemMap:{
        flexDirection:'row',
        alignItems:'center',
    },
    textMap:{
        fontSize:17,
        fontWeight:'bold',
        color:'gray',
        marginLeft:10,
    },
    flatlistAllStore:{
        backgroundColor:'#dbdbdb',
        marginHorizontal:15,
        marginVertical:10,
        borderRadius:10,
    },
    titleAllStore:{
        fontSize:16,
        fontWeight:'bold',
        marginHorizontal:10,
        marginVertical:15,
    },
    listStore:{
        backgroundColor:'white',
        flexDirection:'row',
        marginHorizontal:15,
        borderRadius:10,
        marginVertical:5,
        paddingHorizontal:15,
        paddingVertical:10,
        height:140,
        shadowColor: "#000",
        shadowOffset: {
	    width: 0,
	    height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
    },
    imgStore:{
        width:'30%',
        height:110,
        alignSelf:'center',
        borderRadius:10,
    },
    infoStore:{
        marginHorizontal:10,
    },
    textTitleApp:{
        color:'gray',
        fontSize:13,
        fontWeight:'bold',
        marginVertical:5,
    },
    textAdress:{
        fontSize:18,
        marginRight:90,
    },
})
export default stylesStore;