import {StyleSheet} from 'react-native'
const stylesHome = StyleSheet.create({
    imgCarousel:{
        width: 400, 
        height: 350,
        marginVertical:15,
        borderRadius:10,
        
    },
    container:{
        flex:1,
        flexDirection:'column',
    },
    header:{
        flexDirection:'row',
        paddingHorizontal:15,
        paddingVertical:20,
        backgroundColor:'white',
    },
    logoApp:{
        width:151,
        height:27,
    },
    content:{
        flexDirection:'column',
    },
    carouselContent:{
        
    },
    deliveryMethod:{
        flexDirection:'row',
        marginBottom:15,
    },
    deliveryMethodChild:{
        backgroundColor:'white',
        marginHorizontal:15,
        borderRadius:10,
        width:185,
        height:100,
        shadowColor: "#000",
        shadowOffset: {
	    width: 0,
	    height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
    },
    iconDelivery:{
        width:35,
        height:35,
        borderRadius:20,
        backgroundColor:'#9be9f2',
        alignItems:'center',
        justifyContent:'center',
        marginVertical:15,
        marginHorizontal:15,
    },
    iconAutomaticallyGrab:{
        width:35,
        height:35,
        borderRadius:20,
        backgroundColor:'#ffcf9e',
        alignItems:'center',
        justifyContent:'center',
        marginVertical:15,
        marginHorizontal:15,
    },
    textDeliveryMethod:{
        fontSize:18,
        fontWeight:'bold',
        marginHorizontal:15,
    },
    event:{
       
    },
    imgEvent:{
        width: 400, 
        height: 200,
        borderRadius:10,
        alignSelf:'center',
    },
    footer:{
        flexDirection:'column',
        borderRadius:10,
        backgroundColor:'white',
        marginHorizontal:15,
        marginVertical:15,
    },
    notify:{
        flexDirection:'row',
        paddingVertical:10,
    },
    imgNotify:{
        width:40,
        height:40,
        borderRadius:5,
        marginHorizontal:15,
    },
    titleNotify:{
        borderBottomWidth:0.5,
        borderBottomColor:'#b8b8b8',
    },
    textTitleNotify:{
        fontSize:22,
        fontWeight:'bold',
        paddingVertical:10,
        paddingLeft:15,
    },
})
export default stylesHome;